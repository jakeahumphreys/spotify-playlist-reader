﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Spotify_Playlist_Reader.SpotifyApi.Types;


namespace Spotify_Playlist_Reader
{
    public partial class TrackDetailsForm : Form
    {
        private Track _track { get; set; }
        public TrackDetailsForm(Track track)
        {
            _track = track;
            InitializeComponent();
        }

        private void TrackDetailsForm_Load(object sender, EventArgs e)
        {
            this.Text = _track.name;

            PopulateAbumDetails();
            PopulateTrackDetails();
            PopulateJson();
        }

        private void PopulateAbumDetails()
        {
            pbTrackImage.ImageLocation = _track.album.images.First().url;

            foreach (var image in _track.album.images)
            {
                lstImages.Items.Add(image);
            }

            foreach (var artist in _track.artists)
            {
                lstArtists.Items.Add(artist);
            }



            //Set external url button to external browser
            btnExternalUrl.Text = $"Show in {GetDefaultBrowserName()}";

            //Disable buttons if nessecary
            if (_track.album.uri == null)
                btnAlbumUri.Enabled = false;

            if (_track.album.external_urls.spotify == null)
                btnExternalUrl.Enabled = false;
        }

        private void PopulateTrackDetails()
        {
            if (_track.popularity > 0)
            {
                var popularityNode = treeEnhancedDetails.Nodes.Add("Popularity");
                popularityNode.Nodes.Add(_track.popularity.ToString());
            }

            var idHeaderNode = treeEnhancedDetails.Nodes.Add("IDs");
            var spotifyIdNodeHeader = idHeaderNode.Nodes.Add("Spotify");
            var spotifyIdNode = spotifyIdNodeHeader.Nodes.Add(_track.id);

            if (_track.external_ids != null)
            {
                var isrcHeaderNode = idHeaderNode.Nodes.Add("ISRC");
                var isrcIdNode = isrcHeaderNode.Nodes.Add(_track.external_ids.isrc);
            }

            var durationNode = treeEnhancedDetails.Nodes.Add("Duration");
            var msNodeHeader = durationNode.Nodes.Add("Milliseconds");
            var msNode = msNodeHeader.Nodes.Add(_track.duration_ms.ToString());
            var secondsNodeHeader = durationNode.Nodes.Add("Seconds");
            var secondsNode = secondsNodeHeader.Nodes.Add((_track.duration_ms / 1000).ToString());
            var minutesNodeHeader = durationNode.Nodes.Add("Minutes");
            var minutesNode = minutesNodeHeader.Nodes.Add(Math.Round(((decimal)(_track.duration_ms / 1000) / 60), 1).ToString());

        }

        private void PopulateJson()
        {
            var jsonString = JsonConvert.SerializeObject(_track, Formatting.Indented);
            txtJsonDisplay.Text = jsonString;
        }

        private void lstImages_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedImage = (SpotifyApi.Types.Image)lstImages.SelectedItem;
            pbTrackImage.ImageLocation = selectedImage.url;
        }

        private void btnAlbumUri_Click(object sender, EventArgs e)
        {
            if(_track.album.uri != null)
                System.Diagnostics.Process.Start("explorer.exe", _track.album.uri);
        }

        private void btnExternalUrl_Click(object sender, EventArgs e)
        {
            if(_track.album.external_urls.spotify != null)
                System.Diagnostics.Process.Start("explorer.exe", _track.album.external_urls.spotify);
        }

        private string GetDefaultBrowserName()
        {
            using var key = Registry.CurrentUser.OpenSubKey(
            @"SOFTWARE\Microsoft\Windows\Shell\Associations\URLAssociations\http\UserChoice");
            var progId = (string)key?.GetValue("ProgId");

            switch (progId)
            {
                case "IE.HTTP":
                    return "Internet Explorer";
                case "FirefoxURL":
                    return "Firefox";
                case "ChromeHTML":
                    return "Chrome";
                case "OperaStable":
                    return "Opera";
                case "SafariHTML":
                    return "Safari";
                case "AppXq0fevzme2pys62n3e0fbqa7peapykr8v":
                    return "Edge";
                default:
                    return "Browser";
            }
        }


    }
}
