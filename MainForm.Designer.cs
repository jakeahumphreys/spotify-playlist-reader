﻿namespace Spotify_Playlist_Reader
{
    partial class MainForm : Form
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnApplicationSettings = new System.Windows.Forms.Button();
            this.btnCredentialsManager = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLoadPlaylistFile = new System.Windows.Forms.Button();
            this.txtPlaylistFilePath = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lstPlaylistFileContents = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnTrackDetails = new System.Windows.Forms.Button();
            this.lstTracks = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtTracksListJson = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnRestorePlaylist = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtLogs = new System.Windows.Forms.RichTextBox();
            this.LoadFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnApplicationSettings);
            this.groupBox1.Controls.Add(this.btnCredentialsManager);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 136);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuration";
            // 
            // btnApplicationSettings
            // 
            this.btnApplicationSettings.Location = new System.Drawing.Point(152, 22);
            this.btnApplicationSettings.Name = "btnApplicationSettings";
            this.btnApplicationSettings.Size = new System.Drawing.Size(129, 23);
            this.btnApplicationSettings.TabIndex = 4;
            this.btnApplicationSettings.Text = "Application Settings";
            this.btnApplicationSettings.UseVisualStyleBackColor = true;
            this.btnApplicationSettings.Click += new System.EventHandler(this.btnApplicationSettings_Click);
            // 
            // btnCredentialsManager
            // 
            this.btnCredentialsManager.Location = new System.Drawing.Point(10, 22);
            this.btnCredentialsManager.Name = "btnCredentialsManager";
            this.btnCredentialsManager.Size = new System.Drawing.Size(136, 23);
            this.btnCredentialsManager.TabIndex = 3;
            this.btnCredentialsManager.Text = "Credentials Manager";
            this.btnCredentialsManager.UseVisualStyleBackColor = true;
            this.btnCredentialsManager.Click += new System.EventHandler(this.btnCredentialsManager_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnLoadPlaylistFile);
            this.groupBox3.Controls.Add(this.txtPlaylistFilePath);
            this.groupBox3.Location = new System.Drawing.Point(10, 66);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(756, 64);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "File Selection";
            // 
            // btnLoadPlaylistFile
            // 
            this.btnLoadPlaylistFile.Location = new System.Drawing.Point(6, 22);
            this.btnLoadPlaylistFile.Name = "btnLoadPlaylistFile";
            this.btnLoadPlaylistFile.Size = new System.Drawing.Size(105, 23);
            this.btnLoadPlaylistFile.TabIndex = 0;
            this.btnLoadPlaylistFile.Text = "Load Playlist File";
            this.btnLoadPlaylistFile.UseVisualStyleBackColor = true;
            this.btnLoadPlaylistFile.Click += new System.EventHandler(this.btnLoadPlaylistFile_Click);
            // 
            // txtPlaylistFilePath
            // 
            this.txtPlaylistFilePath.Location = new System.Drawing.Point(117, 22);
            this.txtPlaylistFilePath.Name = "txtPlaylistFilePath";
            this.txtPlaylistFilePath.ReadOnly = true;
            this.txtPlaylistFilePath.Size = new System.Drawing.Size(633, 23);
            this.txtPlaylistFilePath.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 154);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 276);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lstPlaylistFileContents);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 248);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Playlist File Contents (Raw)";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lstPlaylistFileContents
            // 
            this.lstPlaylistFileContents.FormattingEnabled = true;
            this.lstPlaylistFileContents.ItemHeight = 15;
            this.lstPlaylistFileContents.Location = new System.Drawing.Point(6, 6);
            this.lstPlaylistFileContents.Name = "lstPlaylistFileContents";
            this.lstPlaylistFileContents.Size = new System.Drawing.Size(756, 229);
            this.lstPlaylistFileContents.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnTrackDetails);
            this.tabPage2.Controls.Add(this.lstTracks);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 248);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tracks";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnTrackDetails
            // 
            this.btnTrackDetails.Location = new System.Drawing.Point(6, 222);
            this.btnTrackDetails.Name = "btnTrackDetails";
            this.btnTrackDetails.Size = new System.Drawing.Size(75, 23);
            this.btnTrackDetails.TabIndex = 1;
            this.btnTrackDetails.Text = "Details";
            this.btnTrackDetails.UseVisualStyleBackColor = true;
            this.btnTrackDetails.Click += new System.EventHandler(this.btnTrackDetails_Click);
            // 
            // lstTracks
            // 
            this.lstTracks.DisplayMember = "name";
            this.lstTracks.FormattingEnabled = true;
            this.lstTracks.ItemHeight = 15;
            this.lstTracks.Location = new System.Drawing.Point(6, 6);
            this.lstTracks.Name = "lstTracks";
            this.lstTracks.Size = new System.Drawing.Size(756, 214);
            this.lstTracks.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtTracksListJson);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(768, 248);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "JSON";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtTracksListJson
            // 
            this.txtTracksListJson.Location = new System.Drawing.Point(3, 3);
            this.txtTracksListJson.Name = "txtTracksListJson";
            this.txtTracksListJson.ReadOnly = true;
            this.txtTracksListJson.Size = new System.Drawing.Size(762, 242);
            this.txtTracksListJson.TabIndex = 0;
            this.txtTracksListJson.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnRestorePlaylist);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(768, 248);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Actions";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnRestorePlaylist
            // 
            this.btnRestorePlaylist.Enabled = false;
            this.btnRestorePlaylist.Location = new System.Drawing.Point(19, 12);
            this.btnRestorePlaylist.Name = "btnRestorePlaylist";
            this.btnRestorePlaylist.Size = new System.Drawing.Size(75, 23);
            this.btnRestorePlaylist.TabIndex = 0;
            this.btnRestorePlaylist.Text = "Restore ";
            this.btnRestorePlaylist.UseVisualStyleBackColor = true;
            this.btnRestorePlaylist.Click += new System.EventHandler(this.btnRestorePlaylist_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLogs);
            this.groupBox2.Location = new System.Drawing.Point(16, 434);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(768, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application Logs";
            // 
            // txtLogs
            // 
            this.txtLogs.Location = new System.Drawing.Point(6, 22);
            this.txtLogs.Name = "txtLogs";
            this.txtLogs.ReadOnly = true;
            this.txtLogs.Size = new System.Drawing.Size(756, 72);
            this.txtLogs.TabIndex = 0;
            this.txtLogs.Text = "";
            // 
            // LoadFileDialog
            // 
            this.LoadFileDialog.DefaultExt = "txt";
            this.LoadFileDialog.Filter = "Text files (*.txt)|*.txt";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 551);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Spotify Playlist Reader";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private TextBox txtPlaylistFilePath;
        private Button btnLoadPlaylistFile;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private GroupBox groupBox2;
        private RichTextBox txtLogs;
        private OpenFileDialog LoadFileDialog;
        private ListBox lstPlaylistFileContents;
        private Button btnCredentialsManager;
        private GroupBox groupBox3;
        private ListBox lstTracks;
        private Button btnTrackDetails;
        private TabPage tabPage3;
        private RichTextBox txtTracksListJson;
        private Button btnApplicationSettings;
        private TabPage tabPage4;
        private Button btnRestorePlaylist;
    }
}