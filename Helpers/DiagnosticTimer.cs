﻿using Spotify_Playlist_Reader.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.Helpers
{
    
    public static class DiagnosticTimer
    {
        private static Stopwatch stopwatch;

        public static void StartTiming()
        {
            stopwatch = new Stopwatch();   
            stopwatch.Start();
        }

        public static double CurrentTime()
        {
            return stopwatch.Elapsed.TotalSeconds;
        }

        public static double StopTimerAndFetchResults()
        {
            stopwatch.Stop();
            var time = stopwatch.Elapsed.TotalMilliseconds;
            stopwatch.Reset();
            return time;
        }
    }
}
