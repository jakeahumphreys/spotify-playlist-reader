﻿using Spotify_Playlist_Reader.Properties;
using Spotify_Playlist_Reader.SpotifyApi;
using Spotify_Playlist_Reader.SpotifyApi.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spotify_Playlist_Reader
{
    public partial class CredentialsManagementForm : Form
    {
        private readonly SpotifyApiClient _spotifyApiClient;
        public CredentialsManagementForm()
        {
            _spotifyApiClient = new SpotifyApiClient();
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (txtSpotifyApiClientId.Text != Settings.Default.SpotifyClientId)
                Settings.Default.SpotifyClientId = txtSpotifyApiClientId.Text;

            if(txtSpotifyApiClientSecret.Text != Settings.Default.SpotifyClientSecret)
                Settings.Default.SpotifyClientSecret = txtSpotifyApiClientSecret.Text; 

            if(txtSpotifyUserId.Text != Settings.Default.SpotifyUserId)
                Settings.Default.SpotifyUserId = txtSpotifyUserId.Text;

            Settings.Default.Save();

            this.Close();
        }

        private void CredentialsManagementForm_Load(object sender, EventArgs e)
        {
            txtSpotifyApiClientId.Text = Settings.Default.SpotifyClientId;
            txtSpotifyApiClientSecret.Text = Settings.Default.SpotifyClientSecret;
            txtSpotifyUserId.Text = Settings.Default.SpotifyUserId;
        }

        private void btnVerifySpotifyCredentials_Click(object sender, EventArgs e)
        {
            var clientId = txtSpotifyApiClientId.Text;
            var clientSecret = txtSpotifyApiClientSecret.Text;

            var authResponse = _spotifyApiClient.Authenticate(clientId, clientSecret);

            if(authResponse != null && authResponse.AuthenticationToken != null)
            {
                MessageBox.Show("Credentials verified successfully.");
            }
        }
    }
}
