﻿using Spotify_Playlist_Reader.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spotify_Playlist_Reader
{
    public partial class ApplicationSettingsForm : Form
    {
        public ApplicationSettingsForm()
        {
            InitializeComponent();
        }

        private void ApplicationSettingsForm_Load(object sender, EventArgs e)
        {
            if(Settings.Default.EnableDiagnosticTimers)
                cbEnableDiagnosticTimers.Checked = true;
            else
                cbEnableDiagnosticTimers.Checked = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            bool enableTimers;

            if (cbEnableDiagnosticTimers.Checked)
                enableTimers = true;
            else
                enableTimers = false;

            if(Settings.Default.EnableDiagnosticTimers != enableTimers)
                Settings.Default.EnableDiagnosticTimers = enableTimers;

            Settings.Default.Save();    
        }
    }
}
