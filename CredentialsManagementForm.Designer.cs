﻿namespace Spotify_Playlist_Reader
{
    partial class CredentialsManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVerifySpotifyCredentials = new System.Windows.Forms.Button();
            this.txtSpotifyApiClientSecret = new System.Windows.Forms.TextBox();
            this.txtSpotifyApiClientId = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSpotifyUserId = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnVerifySpotifyCredentials);
            this.groupBox1.Controls.Add(this.txtSpotifyApiClientSecret);
            this.groupBox1.Controls.Add(this.txtSpotifyApiClientId);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 111);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spotify Developer API";
            // 
            // btnVerifySpotifyCredentials
            // 
            this.btnVerifySpotifyCredentials.Location = new System.Drawing.Point(304, 80);
            this.btnVerifySpotifyCredentials.Name = "btnVerifySpotifyCredentials";
            this.btnVerifySpotifyCredentials.Size = new System.Drawing.Size(75, 23);
            this.btnVerifySpotifyCredentials.TabIndex = 1;
            this.btnVerifySpotifyCredentials.Text = "Verify";
            this.btnVerifySpotifyCredentials.UseVisualStyleBackColor = true;
            this.btnVerifySpotifyCredentials.Click += new System.EventHandler(this.btnVerifySpotifyCredentials_Click);
            // 
            // txtSpotifyApiClientSecret
            // 
            this.txtSpotifyApiClientSecret.Location = new System.Drawing.Point(6, 51);
            this.txtSpotifyApiClientSecret.Name = "txtSpotifyApiClientSecret";
            this.txtSpotifyApiClientSecret.PlaceholderText = "Client Secret";
            this.txtSpotifyApiClientSecret.Size = new System.Drawing.Size(373, 23);
            this.txtSpotifyApiClientSecret.TabIndex = 1;
            // 
            // txtSpotifyApiClientId
            // 
            this.txtSpotifyApiClientId.Location = new System.Drawing.Point(6, 22);
            this.txtSpotifyApiClientId.Name = "txtSpotifyApiClientId";
            this.txtSpotifyApiClientId.PlaceholderText = "Client ID";
            this.txtSpotifyApiClientId.Size = new System.Drawing.Size(373, 23);
            this.txtSpotifyApiClientId.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(282, 197);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(109, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Save and Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSpotifyUserId);
            this.groupBox2.Location = new System.Drawing.Point(12, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(385, 62);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Spotify User Details";
            // 
            // txtSpotifyUserId
            // 
            this.txtSpotifyUserId.Location = new System.Drawing.Point(6, 22);
            this.txtSpotifyUserId.Name = "txtSpotifyUserId";
            this.txtSpotifyUserId.Size = new System.Drawing.Size(373, 23);
            this.txtSpotifyUserId.TabIndex = 0;
            // 
            // CredentialsManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 229);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox1);
            this.Name = "CredentialsManagementForm";
            this.Text = "Credentials Management";
            this.Load += new System.EventHandler(this.CredentialsManagementForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private Button btnVerifySpotifyCredentials;
        private TextBox txtSpotifyApiClientSecret;
        private TextBox txtSpotifyApiClientId;
        private Button btnClose;
        private GroupBox groupBox2;
        private TextBox txtSpotifyUserId;
    }
}