﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.Types.Error_Handling
{
    public static class ErrorBecause
    {
        public static Error UnableToEncodeCredentials(string clientId, string clientSecret)
        {
            return new Error
            {
                TechnicalMessage = $"Unable to encode [{clientId}:{clientSecret}] as a base64 string. ",
                UserMessage = "Unable to encode credentials. Check stored credentials are valid."
            };
        }

        public static Error NoResponseFromSpotifyApi()
        {
            return new Error
            {
                TechnicalMessage = $"Internally, the response object was null.",
                UserMessage = "No response from Spotify API."
            };
        }
    }
}
