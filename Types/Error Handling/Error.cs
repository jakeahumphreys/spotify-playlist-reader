﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.Types.Error_Handling
{
    public class Error
    {
        public string UserMessage { get; set; }
        public string TechnicalMessage { get; set; }
    }
}
