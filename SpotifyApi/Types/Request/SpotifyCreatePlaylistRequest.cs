﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.SpotifyApi.Types.Request
{
    public class SpotifyCreatePlaylistRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Public { get; set; }
    }
}
