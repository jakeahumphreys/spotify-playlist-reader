﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.SpotifyApi.Types.Response
{
    public class TrackResponse
    {
        public List<Track> Tracks { get; set; }
    }
}
