﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.SpotifyApi.Types
{
    public class SpotifyAuthenticationRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
