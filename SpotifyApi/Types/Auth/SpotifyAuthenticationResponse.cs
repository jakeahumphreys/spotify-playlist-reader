﻿using Spotify_Playlist_Reader.Types.Error_Handling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotify_Playlist_Reader.SpotifyApi.Types
{
    public class SpotifyAuthenticationResponse
    {
        public string? AuthenticationToken { get; set; }
        public bool HasError { get; set; }
        public List<Error>? Errors { get; set; }

        public static SpotifyAuthenticationResponse FromError(Error error)
        {
            return new SpotifyAuthenticationResponse
            {
                HasError = true,
                Errors = new List<Error>
                {
                    error
                }
            };
        }
    }
}
