﻿namespace Spotify_Playlist_Reader.SpotifyApi.Types
{
    public class Image
    {
        public int height { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }


}
