﻿using Newtonsoft.Json;
using Spotify_Playlist_Reader.Properties;
using Spotify_Playlist_Reader.SpotifyApi.Types;
using Spotify_Playlist_Reader.SpotifyApi.Types.Request;
using Spotify_Playlist_Reader.SpotifyApi.Types.Response;
using Spotify_Playlist_Reader.Types.Error_Handling;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Spotify_Playlist_Reader.SpotifyApi
{
    public class SpotifyApiClient
    {
        private HttpClient _httpClient;
        public SpotifyApiClient()
        {
            _httpClient = new HttpClient();
        }
        public SpotifyAuthenticationResponse Authenticate(string clientId, string clientSecret)
        {
            var base64EncodedCredentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}"));

            var postData = new Dictionary<string, string>();

            postData.Add("grant_type", "client_credentials");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedCredentials);

            var authRequest = new HttpRequestMessage(HttpMethod.Post, "https://accounts.spotify.com/api/token")
            {
                Content = new FormUrlEncodedContent(postData)
            };

            var authResponse = _httpClient.SendAsync(authRequest).Result;

            var result = JsonConvert.DeserializeObject<SpotifyApiClientFlowAuthResponse>(authResponse.Content.ReadAsStringAsync().Result);

            if (result == null)
                return SpotifyAuthenticationResponse.FromError(ErrorBecause.NoResponseFromSpotifyApi());

            return new SpotifyAuthenticationResponse { AuthenticationToken = result.AccessToken };
        }

        public void CreatePlaylistFromContents()
        {
            var authResponse = Authenticate(Settings.Default.SpotifyClientId, Settings.Default.SpotifyClientSecret);

            var userId = Settings.Default.SpotifyUserId;

            var createPlaylistRequest = new SpotifyCreatePlaylistRequest
            {
                Name = "SPR Restored Playlist",
                Description = "Restored via SPR",
                Public = false
            };

            var uriBuilder = new UriBuilder($"https://api.spotify.com/v1/users/{userId}/playlists");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authResponse.AuthenticationToken);

            var content = new StringContent(JsonConvert.SerializeObject(createPlaylistRequest));

            var response = _httpClient.PostAsync(uriBuilder.ToString(), content).Result;

        }

        public List<Track> GetSeveralTracks(List<string> trackIds)
        {
            var authResponse = Authenticate(Settings.Default.SpotifyClientId, Settings.Default.SpotifyClientSecret);

            var trackIdsString = BuildTrackIdsString(trackIds);

            var uriBuilder = new UriBuilder("https://api.spotify.com/v1/tracks");

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["ids"] = trackIdsString;
            uriBuilder.Query = query.ToString();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authResponse.AuthenticationToken);

            var response = _httpClient.GetAsync(uriBuilder.ToString()).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;

            var trackResponse = JsonConvert.DeserializeObject<TrackResponse>(responseString);

            return trackResponse.Tracks;
        }

        private string BuildTrackIdsString(List<string> trackIds)
        {
            StringBuilder stringBuilder = new StringBuilder();

            for(var i = 0; i < trackIds.Count; i++)
            {
                if(i != trackIds.Count-1)
                    stringBuilder.Append($"{trackIds[i]},");
                else
                    stringBuilder.Append(trackIds[i]);
            }

            return stringBuilder.ToString();
        }
    }
}
