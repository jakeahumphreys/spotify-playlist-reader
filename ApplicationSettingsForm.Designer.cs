﻿namespace Spotify_Playlist_Reader
{
    partial class ApplicationSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbEnableDiagnosticTimers = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbEnableDiagnosticTimers);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(524, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Diagnostics";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(461, 132);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbEnableDiagnosticTimers
            // 
            this.cbEnableDiagnosticTimers.AutoSize = true;
            this.cbEnableDiagnosticTimers.Location = new System.Drawing.Point(11, 22);
            this.cbEnableDiagnosticTimers.Name = "cbEnableDiagnosticTimers";
            this.cbEnableDiagnosticTimers.Size = new System.Drawing.Size(158, 19);
            this.cbEnableDiagnosticTimers.TabIndex = 0;
            this.cbEnableDiagnosticTimers.Text = "Enable Diagnostic Timers";
            this.cbEnableDiagnosticTimers.UseVisualStyleBackColor = true;
            // 
            // ApplicationSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 167);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox1);
            this.Name = "ApplicationSettingsForm";
            this.Text = "Application Settings";
            this.Load += new System.EventHandler(this.ApplicationSettingsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private CheckBox cbEnableDiagnosticTimers;
        private Button btnSave;
    }
}