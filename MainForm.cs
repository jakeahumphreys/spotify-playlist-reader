using Newtonsoft.Json;
using Spotify_Playlist_Reader.Helpers;
using Spotify_Playlist_Reader.Properties;
using Spotify_Playlist_Reader.SpotifyApi;
using Spotify_Playlist_Reader.SpotifyApi.Types;

namespace Spotify_Playlist_Reader
{
    public partial class MainForm : Form
    {
        private readonly SpotifyApiClient _spotifyApiClient;
        public MainForm()
        {
            _spotifyApiClient = new SpotifyApiClient();
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CheckSpotifyCredentialsExist();
        }

        private void CheckSpotifyCredentialsExist()
        {

            txtLogs.AppendText("Checking Stored Credentials... \n");

            var spotifyClientId = Settings.Default.SpotifyClientId;
            var spotifyClientSecret = Settings.Default.SpotifyClientSecret;

            if (string.IsNullOrEmpty(spotifyClientId))
                txtLogs.AppendText("Spotify Client ID [Missing!] \n");
            else
                txtLogs.AppendText("Spotify Client ID [Found!] \n");
            
            if (string.IsNullOrEmpty(spotifyClientSecret))
                txtLogs.AppendText("Spotify Client Secret [Missing!] \n");
            else
                txtLogs.AppendText("Spotify Client Secret [Found!] \n");

            if (!string.IsNullOrEmpty(spotifyClientId) && !string.IsNullOrEmpty(spotifyClientSecret))
                VerifySpotifyCredentialsOnLoad(spotifyClientId, spotifyClientSecret);
        }

        private void VerifySpotifyCredentialsOnLoad(string clientId, string clientSecret)
        {
            DiagnosticsStart("SpotifyCredentialsVerification");

            var authResponse = _spotifyApiClient.Authenticate(clientId, clientSecret);

            if (authResponse.AuthenticationToken == null)
                AddLog("Stored Spotify credentials are invalid", Color.Red);
            else
                AddLog($"Spotify Credentials validated successfully, returned token: \n[{authResponse.AuthenticationToken}]", Color.Green);

            DiagnosticsStop();
        }

        private void btnLoadPlaylistFile_Click(object sender, EventArgs e)
        {
            DiagnosticsStart("PlaylistFileLoad");

            LoadFileDialog.ShowDialog();

            if(!string.IsNullOrEmpty(LoadFileDialog.FileName))
            {
                var playlistFilePath = LoadFileDialog.FileName;
                txtPlaylistFilePath.Text = playlistFilePath;

                var playlistContents = GetPlaylistContents(playlistFilePath);

                if(playlistContents != null && playlistContents.Count > 0)
                {
                    SetPlaylistContentsTab(playlistContents);
                    SetTracksTab(playlistContents);
                }
            }

            DiagnosticsStop();
        }

        private List<string>? GetPlaylistContents(string filePath)
        {
            DiagnosticsStart("FetchPlaylistContents");

            AddLog($"Loading contents from file {filePath}...");
            if(!string.IsNullOrEmpty(filePath))
            {
                var playlistFileContents = File.ReadAllLines(filePath).ToList();

                if (playlistFileContents == null || playlistFileContents.Count == 0)
                {
                    AddLog("Playlist File is empty.", Color.Red);
                    DiagnosticsStop();
                    return null;
                }
                else
                {
                    AddLog($"Fetched {playlistFileContents.Count} items.");
                    DiagnosticsStop();
                    return playlistFileContents;
                } 
            }
            else
            {
                AddLog("No Filepath.", Color.Red);
                DiagnosticsStop();
                return null;
            }
        }

        private void SetPlaylistContentsTab(List<string> playlistContents)
        {
            DiagnosticsStart("SetContentsTab");

            lstPlaylistFileContents.Items.Clear();

            for(int i = 0; i < playlistContents.Count; i++)
            {
                lstPlaylistFileContents.Items.Add(playlistContents[i]);
            }

            DiagnosticsStop();
        }

       private void SetTracksTab(List<string> playlistContents)
       {
            DiagnosticsStart("SetTracksTab");

            var trackIds = playlistContents.Select(x => x.Replace("https://open.spotify.com/track/", ""));

            var tracks = new List<Track>(playlistContents.Count);

            string[][] chunks = trackIds
                .Select((s, i) => new { Value = s, Index = i })
                .GroupBy(x => x.Index / 50)
                .Select(grp => grp.Select(x => x.Value).ToArray())
                .ToArray();

            foreach (var chunkArray in chunks)
            {
                var sortedTrackIds = new List<string>();
                foreach (var trackId in chunkArray)
                {
                    sortedTrackIds.Add(trackId);
                }

                var foundTracks = _spotifyApiClient.GetSeveralTracks(sortedTrackIds);

                foreach (var track in foundTracks)
                {
                    tracks.Add(track);
                }
            }

            lstTracks.Items.Clear();

            foreach(var track in tracks)
            {
                lstTracks.Items.Add(track);
            }

            SetTracksListJsonTab(tracks);

            DiagnosticsStop();
        }

        private void SetTracksListJsonTab(List<Track> tracks)
        {
            DiagnosticsStart("SetTracksListJsonTab");

            var json = JsonConvert.SerializeObject(tracks, Formatting.Indented);
            txtTracksListJson.Text = json;

            DiagnosticsStop();
        }

        private void AddLog(string text)
        {
            txtLogs.SelectionStart = txtLogs.Text.Length;
            txtLogs.SelectionLength = 0;
            txtLogs.AppendText(text += "\n");
        }

        private void AddLog(string text, Color color)
        {
            txtLogs.SelectionStart = txtLogs.Text.Length;
            txtLogs.SelectionLength = 0;
            txtLogs.SelectionColor = color;
            txtLogs.AppendText(text += "\n");
            txtLogs.SelectionColor = txtLogs.ForeColor;
        }

        private void btnCredentialsManager_Click(object sender, EventArgs e)
        {
            var credentialsForm = new CredentialsManagementForm();
            credentialsForm.Show();
        }

        private void btnTrackDetails_Click(object sender, EventArgs e)
        {
            var trackDetailsForm = new TrackDetailsForm((Track)lstTracks.SelectedItem);
            trackDetailsForm.Show();    
        }

        private void DiagnosticsStart(string operation)
        {
            if(Settings.Default.EnableDiagnosticTimers)
            {
                AddLog($"(Diagnostics) Starting timer for operation: {operation}...", Color.Blue);
                DiagnosticTimer.StartTiming();
            } 
        }

        private void DiagnosticsStop()
        {
            if (Settings.Default.EnableDiagnosticTimers)
            {
                var elapsedMs = DiagnosticTimer.StopTimerAndFetchResults();
                AddLog($"(Diagnostics) Timer stopped, method took {elapsedMs}.", Color.Blue);
            }
        }

        private void btnApplicationSettings_Click(object sender, EventArgs e)
        {
            var applicationSettingsForm = new ApplicationSettingsForm();
            applicationSettingsForm.Show();
        }

        private void btnRestorePlaylist_Click(object sender, EventArgs e)
        {
            _spotifyApiClient.CreatePlaylistFromContents();
        }
    }
}