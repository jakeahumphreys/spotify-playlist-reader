﻿namespace Spotify_Playlist_Reader
{
    partial class TrackDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnExternalUrl = new System.Windows.Forms.Button();
            this.btnAlbumUri = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.pbTrackImage = new System.Windows.Forms.PictureBox();
            this.lstImages = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeEnhancedDetails = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstArtists = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtJsonDisplay = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTrackImage)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(700, 492);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(692, 464);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.treeView1);
            this.groupBox3.Controls.Add(this.pbTrackImage);
            this.groupBox3.Controls.Add(this.lstImages);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(680, 209);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Album";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnExternalUrl);
            this.groupBox4.Controls.Add(this.btnAlbumUri);
            this.groupBox4.Location = new System.Drawing.Point(416, 18);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(258, 182);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Album Actions";
            // 
            // btnExternalUrl
            // 
            this.btnExternalUrl.Location = new System.Drawing.Point(6, 51);
            this.btnExternalUrl.Name = "btnExternalUrl";
            this.btnExternalUrl.Size = new System.Drawing.Size(246, 23);
            this.btnExternalUrl.TabIndex = 1;
            this.btnExternalUrl.Text = "Show in Browser";
            this.btnExternalUrl.UseVisualStyleBackColor = true;
            this.btnExternalUrl.Click += new System.EventHandler(this.btnExternalUrl_Click);
            // 
            // btnAlbumUri
            // 
            this.btnAlbumUri.Location = new System.Drawing.Point(6, 22);
            this.btnAlbumUri.Name = "btnAlbumUri";
            this.btnAlbumUri.Size = new System.Drawing.Size(246, 23);
            this.btnAlbumUri.TabIndex = 0;
            this.btnAlbumUri.Text = "Show in Spotify";
            this.btnAlbumUri.UseVisualStyleBackColor = true;
            this.btnAlbumUri.Click += new System.EventHandler(this.btnAlbumUri_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(156, 18);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(254, 182);
            this.treeView1.TabIndex = 4;
            // 
            // pbTrackImage
            // 
            this.pbTrackImage.Location = new System.Drawing.Point(6, 18);
            this.pbTrackImage.Name = "pbTrackImage";
            this.pbTrackImage.Size = new System.Drawing.Size(137, 127);
            this.pbTrackImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTrackImage.TabIndex = 0;
            this.pbTrackImage.TabStop = false;
            // 
            // lstImages
            // 
            this.lstImages.DisplayMember = "url";
            this.lstImages.FormattingEnabled = true;
            this.lstImages.ItemHeight = 15;
            this.lstImages.Location = new System.Drawing.Point(6, 151);
            this.lstImages.Name = "lstImages";
            this.lstImages.Size = new System.Drawing.Size(137, 49);
            this.lstImages.TabIndex = 3;
            this.lstImages.SelectedIndexChanged += new System.EventHandler(this.lstImages_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.treeEnhancedDetails);
            this.groupBox2.Location = new System.Drawing.Point(295, 221);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(391, 127);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Track Details";
            // 
            // treeEnhancedDetails
            // 
            this.treeEnhancedDetails.FullRowSelect = true;
            this.treeEnhancedDetails.Location = new System.Drawing.Point(6, 22);
            this.treeEnhancedDetails.Name = "treeEnhancedDetails";
            this.treeEnhancedDetails.Size = new System.Drawing.Size(379, 97);
            this.treeEnhancedDetails.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstArtists);
            this.groupBox1.Location = new System.Drawing.Point(6, 221);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 127);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Artists";
            // 
            // lstArtists
            // 
            this.lstArtists.DisplayMember = "name";
            this.lstArtists.FormattingEnabled = true;
            this.lstArtists.ItemHeight = 15;
            this.lstArtists.Location = new System.Drawing.Point(6, 22);
            this.lstArtists.Name = "lstArtists";
            this.lstArtists.Size = new System.Drawing.Size(271, 94);
            this.lstArtists.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtJsonDisplay);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(692, 464);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "JSON";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtJsonDisplay
            // 
            this.txtJsonDisplay.Location = new System.Drawing.Point(6, 6);
            this.txtJsonDisplay.Name = "txtJsonDisplay";
            this.txtJsonDisplay.ReadOnly = true;
            this.txtJsonDisplay.Size = new System.Drawing.Size(680, 452);
            this.txtJsonDisplay.TabIndex = 0;
            this.txtJsonDisplay.Text = "";
            // 
            // TrackDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 528);
            this.Controls.Add(this.tabControl1);
            this.Name = "TrackDetailsForm";
            this.Text = "TrackDetailsForm";
            this.Load += new System.EventHandler(this.TrackDetailsForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbTrackImage)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private RichTextBox txtJsonDisplay;
        private PictureBox pbTrackImage;
        private GroupBox groupBox1;
        private ListBox lstArtists;
        private GroupBox groupBox2;
        private TreeView treeEnhancedDetails;
        private ListBox lstImages;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private TreeView treeView1;
        private Button btnAlbumUri;
        private Button btnExternalUrl;
    }
}