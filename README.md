# Spotify Playlist Reader (WIP) 🎵

### About
This is a simple application designed to read the contents of a Spotify playlist from a .txt file. There is a feature that exists in the Spotify desktop application wherein you can copy and paste the contents of a playlist into another location e.g. text file, However when it does so it converts the playlist contents into Spotify URLs with Track IDs. This application loads these URLs into memory, then uses the Spotify API to convert the track URLs into Track objects containing all the song data held on the API.

### Spotify API
Currently the application integrates with the [Spotify Developer API](https://developer.spotify.com/ "Spotify Developer API") via the Client Credentials Flow. This isnt the preferable solution and will eventually be replaced with **OAuth 2.0**.

### Technologies
- .NET 6.0
- Windows Forms

### External Libraries
- [Newtonsoft.Json](https://www.newtonsoft.com/json "Newtonsoft.Json")

### Features
- Convert a list of Spotify URLs to an end user viewable list of songs
- View JSON for individual Tracks and lists of Tracks
- View API held data for a spotify Track

### Potential Features
- Re-import a playlist back into the users Spotify account.
- Direct snapshotting from a users Spotify account.
- Application Statistics.

